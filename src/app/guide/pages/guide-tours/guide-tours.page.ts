import { Component, OnInit } from '@angular/core';
import { TourModel } from 'src/core/models/tour.model';
import { TOURS_LIST } from 'src/core/mocks/tours.mock';

@Component({
  selector: 'app-guide-tours',
  templateUrl: './guide-tours.page.html',
  styleUrls: ['./guide-tours.page.scss'],
})
export class GuideToursPage implements OnInit {
  public tours: TourModel[];

  constructor() { }

  ngOnInit() {
    this.tours = TOURS_LIST;
  }

}
