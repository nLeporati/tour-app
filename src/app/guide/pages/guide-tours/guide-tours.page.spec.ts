import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuideToursPage } from './guide-tours.page';

describe('GuideToursPage', () => {
  let component: GuideToursPage;
  let fixture: ComponentFixture<GuideToursPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuideToursPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuideToursPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
