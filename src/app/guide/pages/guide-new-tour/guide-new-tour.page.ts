import { Component, OnInit } from '@angular/core';
import { TourModel } from 'src/core/models/tour.model';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-guide-new-tour',
  templateUrl: './guide-new-tour.page.html',
  styleUrls: ['./guide-new-tour.page.scss'],
})
export class GuideNewTourPage implements OnInit {
  public step: number;
  public tour: TourModel[];
  public image: any;

  constructor(private platfrm: Platform) { }

  async ngOnInit() {
    // await this.platfrm.ready();
    this.step = 1;
  }

  public changeImage(event): void {
    console.log(event);
    console.log(event.target.childNodes[1].files);
    this.readFile(event.target.childNodes[1].files[0]);
  }

  private readFile(file) {
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
      console.log(this.image);
    };
    myReader.readAsDataURL(file);
  }

}
