import { Component, OnInit } from '@angular/core';
import { TourModel } from 'src/core/models/tour.model';
import { TOURS_LIST } from 'src/core/mocks/tours.mock';
import { USERS_LIST } from 'src/core/mocks/users.mock';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Platform } from '@ionic/angular';
import { MapService } from 'src/core/services/map.service';
import { environment } from 'src/environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-guide',
  templateUrl: './guide.page.html',
  styleUrls: ['./guide.page.scss'],
})
export class GuidePage implements OnInit {
  public tours: TourModel[];
  public activeTour: TourModel;
  public users: any[];
  public status = 'INACTIVE';
  public scanning: boolean;

  constructor(
    private mapService: MapService,
    private platform: Platform,
    private qrScanner: QRScanner,
    private afs: AngularFirestore
  ) { }

  async ngOnInit() {
    await this.platform.ready();
    this.tours = TOURS_LIST;
    this.users = [];
  }

  ionViewWillLeave() {
    this.qrScanner.destroy();
  }

  getUsers() {
    this.afs.doc(`map_tours/${this.activeTour.fid}`).collection('users').valueChanges().subscribe(
      users => {
        console.log(users);
        users.forEach(user => {
          user.status = 'JOINED';
          this.users.push(user);
        });
      }
    );
  }

  activateTour(tour: TourModel) {
    this.status = 'PAUSE';
    this.activeTour = tour;
    this.activeTour.status = this.status;
    this.mapService.updateTourStatus(this.activeTour);
    this.getUsers();
  }

  public scann() {
    console.log('scanning');
    this.mapService.addUserToTour(this.activeTour, environment.user);
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted
          // start scanning
          this.scanning = true;

          const scanSub = this.qrScanner.scan().subscribe((fid: string) => {
            const user = this.users.find(u => u.fid === fid);
            if (!this.users.some(u => u.fid === fid)) {
              user.status = 'CONFIRMED';
              // this.mapService.updateTourStatus(this.activeTour);
              // this.mapService.addUserToTour(this.activeTour, environment.user);
            }
            scanSub.unsubscribe(); // stop scanning
            this.stopScanning();
          });

        } else if (status.denied) {
          alert('Se deben conceder los permisos para utilizar la camara.');
        } else {
          alert('Se deben conceder los permisos para utilizar la camara.');
        }
      })
      .catch((e: any) => {
        console.error('QRScanner:', e);
        // this.confirmTour();
      })
      .finally(() => {
        // this.confirmTour();
      });
  }

  stopScanning() {
    if (this.scanning) {
      this.qrScanner.destroy(); // hide camera preview
      this.scanning = false;
    }
  }

  startTour() {
    this.status = 'ACTIVE';
  }

  finishTour() {
    this.status = 'PAUSE';
    this.activeTour = null;
  }
}
