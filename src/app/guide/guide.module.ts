import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GuidePage } from './guide.page';
import { GuideToursPage } from './pages/guide-tours/guide-tours.page';
import { GuideNewTourPage } from './pages/guide-new-tour/guide-new-tour.page';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';

const routes: Routes = [
  {
    path: '',
    component: GuidePage
  },
  {
    path: 'tours',
    component: GuideToursPage
  },
  {
    path: 'new',
    component: GuideNewTourPage
  }
];

@NgModule({
  providers: [
    QRScanner
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    GuidePage,
    GuideToursPage,
    GuideNewTourPage
  ]
})
export class GuidePageModule {}
