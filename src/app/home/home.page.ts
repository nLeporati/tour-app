import { Component, OnInit, ViewChild } from '@angular/core';
import { Platform, IonInput } from '@ionic/angular';
import { GoogleMaps, GoogleMap } from '@ionic-native/google-maps';
import { TourModel } from 'src/core/models/tour.model';
import { TOURS_LIST } from 'src/core/mocks/tours.mock';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild(IonInput) public searchInput: IonInput;
  public map: GoogleMap;
  public searching: boolean;
  public data: TourModel[];

  constructor(private readonly platform: Platform) {}

  async ngOnInit() {
    await this.platform.ready();
    this.data = TOURS_LIST;
    // await this.loadMap();
  }

  private loadMap() {
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 43.0741704,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    });
  }

  public search() {
    this.searching = !this.searching;
  }

  public viewDetails() {
    
  }
}
