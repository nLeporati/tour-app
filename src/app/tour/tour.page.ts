import { Component, OnInit, ViewChild } from '@angular/core';
import { Platform, IonInput, ToastController, AlertController } from '@ionic/angular';
import { GoogleMaps, GoogleMap, GoogleMapOptions, Marker, GoogleMapsEvent, Polyline, ILatLng, Circle, HtmlInfoWindow } from '@ionic-native/google-maps';
import { TourModel } from 'src/core/models/tour.model';
import { TOURS_LIST } from 'src/core/mocks/tours.mock';
import { TourSessionService } from 'src/core/services/tour-session.service';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GeoPositionModel } from 'src/core/models/geo-point.model';
import { Router } from '@angular/router';
import { MapService } from 'src/core/services/map.service';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserModel } from 'src/core/models/user.model';

@Component({
  selector: 'app-tour',
  templateUrl: 'tour.page.html',
  styleUrls: ['tour.page.scss'],
})
export class TourPage implements OnInit {
  @ViewChild(IonInput) public searchInput: IonInput;
  public authUser: UserModel;
  private polyline: Polyline;
  private position$: Subscription;
  public map: GoogleMap;
  public showContent: boolean;
  public searching: boolean;
  public data: TourModel[];
  public markers: Marker[];
  public scanning: boolean;
  public coords: GeoPositionModel;
  public showQRCode: boolean;
  public loadingDirection: boolean;
  public htmlInfoWindow: HtmlInfoWindow;
  public circle: Circle;

  constructor(
    private readonly platform: Platform,
    private readonly router: Router,
    public tourSession: TourSessionService,
    private qrScanner: QRScanner,
    public toastController: ToastController,
    public alertController: AlertController,
    private geolocation: Geolocation,
    private mapService: MapService
  ) {}

  async ngOnInit() {
    await this.platform.ready();
    this.authUser = environment.user;
    this.data = TOURS_LIST;
    await this.loadGeolocation();
  }

  ionViewWillEnter() {
    this.showContent = false;
    if (this.tourSession.active) {
      this.getDirection();
      this.watchTour();
    }
    if (this.coords) {
      this.watchPosition();
    }
  }

  ionViewWillLeave() {
    // Destroy QRScanner if is active
    if (this.qrScanner) {
      this.qrScanner.destroy();
      this.scanning = false;
    }
    // cLose InfoWindow if is open
    if (this.htmlInfoWindow) {
      this.htmlInfoWindow.close();
      this.htmlInfoWindow = null;
    }
    this.position$.unsubscribe();
  }

  loadGeolocation() {
    setTimeout(() => {
      if (!this.coords) {
        this.coords = new GeoPositionModel(-33.444447, -70.655427);
        this.mapService.updatePosition(Object.assign({}, this.coords));
        this.loadMap();
        this.watchPosition();
      }
    }, 1);

    // this.geolocation.getCurrentPosition().then((resp) => {
    //   this.coords = new GeoPositionModel(resp.coords.latitude, resp.coords.longitude);
    //   this.mapService.updatePosition(Object.assign({}, this.coords));
    //   this.loadMap();
    //   this.watchPosition();
    // }).catch((error) => {
    //   alert('Se debe activar lo localización GPS');
    // });
  }

  private watchPosition() {
    this.position$ = this.geolocation.watchPosition().subscribe((data) => {
      console.log('watch', data);
      if (data.coords) {
        // Update position
        this.coords = new GeoPositionModel(data.coords.latitude, data.coords.longitude);
        this.markers[0].setPosition(this.coords);
        this.map.setCameraTarget(this.coords);
        this.setRadius();
        this.mapService.updatePosition(Object.assign({}, this.coords));
      }
    });
  }

  watchTour() {
    this.tourSession.watchTour().subscribe(
      (res) => {
        console.log(res);
        if (res.status === 'PAUSE') {
          this.confirmTour();
        }
      }
    );
  }

  async loadMap() {
    const mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.coords.lat,
          lng: this.coords.lng
        },
        zoom: 15,
        tilt: 30
      }
    };
    this.map = GoogleMaps.create('map_canvas', mapOptions);
    await this.setRadius();
    await this.addMarkers();
  }

  private async setRadius() {
    const distance = 700;
    // this.data = this.data.filter(tour => {
    //   const radius = Spherical.computeDistanceBetween(this.coords, tour.position);
    //   return (radius <= distance);
    // });

    if (this.circle) {
      this.circle.setCenter(this.coords);
    } else {
      this.circle = await this.map.addCircleSync({
        center: this.coords,
        radius: distance,
        strokeColor : '#AA00FF',
        strokeWidth: 2,
        fillColor : '#00880055'
      });
    }
  }

  private async addMarkers() {
    this.markers = new Array<Marker>();

    await this.markers.push(this.map.addMarkerSync({
      title: 'You!',
      icon: 'red',
      animation: 'DROP',
      position: {
        lat: this.coords.lat,
        lng: this.coords.lng
      },
    }));

    this.data.forEach((tour, i) => {
      this.map.addMarker({
        // title: tour.title,
        icon: 'blue',
        animation: 'DROP',
        position: tour.position
      })
      .then((marker) => {
        this.markers[i + 1] = marker;
        this.markers[i + 1].on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          this.createInfoWindow(this.markers[i + 1], tour);
        });
      });

      this.mapService.getTour(tour.fid).subscribe(
        (res) => {
          console.log(res);
          if (res && this.markers[i + 1]) {
            const position: ILatLng = { lat: res.lat, lng: res.lng };
            tour.status = res.status;
            tour.position = position;
            this.markers[i + 1].setPosition(position);
          }
        }
      );
    });

  }

  createInfoWindow(marker: Marker, tour: TourModel) {
    this.htmlInfoWindow = new HtmlInfoWindow();
    const frame: HTMLElement = document.createElement('div');
    frame.innerHTML = [
      `<div style="display: flex; align-items: center; justify-content: space-between; width: 97%">
        <h4>${tour.title}</h4><span>3/5</span></ion-icon>
      </div>`,
      `<p style="margin-top: 0px; margin-bottom: 2px">${tour.guide.name}</p>`,
      `<img style="width: 230px; margin-bottom: 5px" src="${tour.images[0]}">`,
    ].join('');

    frame.getElementsByTagName('img')[0].addEventListener('click', () => {
      this.router.navigate(['tour/details/', tour.id]);
      this.htmlInfoWindow.close();
    });

    this.htmlInfoWindow.setContent(frame, {
      width: '220px',
      height: '230px',
    });

    this.htmlInfoWindow.open(marker);
  }

  public async getDirection() {
    if (this.loadingDirection) {
      return;
    }
    this.loadingDirection = true;
    const origin = { lat: this.coords.lat, lng: this.coords.lng };
    const destination = this.tourSession.tour.position;
    const points = await this.mapService.getDirectionAsync(origin, destination);

    if (this.polyline) {
      this.polyline.setPoints(points);
    } else {
      await this.addDirection(points);
    }
    this.loadingDirection = false;
  }

  public async addDirection(pointsList: ILatLng[]) {
    this.polyline = await this.map.addPolylineSync({
      points: pointsList,
      color: '#AA00FF',
      width: 10,
      geodesic: true,
      clickable: false  // clickable = false in default
    });
  }

  public toggleShowContent() {
    if (!this.tourSession.status) {
      this.showContent = !this.showContent;
    }
  }

  public toggleQRCode() {
    this.showQRCode = !this.showQRCode;
  }

  public startScanning() {
  // Optionally request the permission early
  this.qrScanner.prepare()
    .then((status: QRScannerStatus) => {
      if (status.authorized) {
        // camera permission was granted
        // start scanning
        this.scanning = true;
        this.map.setClickable(false);

        const scanSub = this.qrScanner.scan().subscribe((text: string) => {
          // this.tourSession.status = 'pause';
          // this.map.setClickable(true);
          // this.confirmTour();
          scanSub.unsubscribe(); // stop scanning
          this.qrScanner.destroy(); // hide camera preview
          // this.scanning = false;
          // this.showQRCode = false;
          // this.presentToast('Tour confirmado');
        });

      } else if (status.denied) {
        // camera permission was permanently denied
        // you must use QRScanner.openSettings() method to guide the user to the settings page
        // then they can grant the permission from there
        alert('Se deben conceder los permisos para utilizar la camara.');
      } else {
        // permission was denied, but not permanently. You can ask for permission again at a later time.
        alert('Se deben conceder los permisos para utilizar la camara.');
      }
    })
    .catch((e: any) => {
      console.error('QRScanner:', e);
      this.confirmTour();
    })
    .finally(() => {
      this.confirmTour();
    });
  }

  public stopScanning() {
    if (this.scanning) {
      this.qrScanner.destroy(); // hide camera preview
      this.scanning = false;

    } else if (this.showQRCode) {
      this.showQRCode = false;
    }
  }

  public confirmTour() {
    this.presentToast('Te has unido al Tour');
    this.scanning = false;
    this.showQRCode = false;
    // this.tourSession.updateTourStatus('PAUSE');
    this.map.setClickable(true);
    this.polyline.setVisible(false);
    this.polyline = null;
  }

  async cancelTour() {
    const alert = await this.alertController.create({
      header: 'Cancelar Tour',
      message: '¿Estas seguro de cancelar el tour activo?',
      buttons: [
        {
          text: 'Continuar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            if (this.tourSession.status === 'pause') {
              this.tourSession.status = 'active';
              this.router.navigate(['tour/active']);
            }
          }
        }, {
          text: 'Si, deseo cancelar',
          handler: () => {
            alert.dismiss();
            this.cancelActivedTour();
          }
        }
      ]
    });
    await alert.present();
  }

  private cancelActivedTour() {
    this.tourSession.removeActivedTour();
    this.polyline.setVisible(false);
    this.polyline = null;
    this.presentToast('El Tour a sido cancelado');
  }

  public async finalizeTour() {
    this.tourSession.removeActivedTour();
    const alert = await this.alertController.create({
      header: 'Tour terminado',
      message: '¿Quieres calificar el guíado? Seria de mucha ayuda!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Calificar'
        }
      ]
    });
    await alert.present();
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  public changeCircle() {
    if (this.circle) {
      this.circle.remove();
      this.circle = null;
    } else {
      this.setRadius();
    }
  }

  public openSearchPage(): void {
    this.router.navigate(['/tour/search']);
  }
}
