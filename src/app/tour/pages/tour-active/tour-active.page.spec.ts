import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourActivePage } from './tour-active.page';

describe('TourActivePage', () => {
  let component: TourActivePage;
  let fixture: ComponentFixture<TourActivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourActivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourActivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
