import { Component, OnInit } from '@angular/core';
import { TourSessionService } from 'src/core/services/tour-session.service';
import { TourModel } from 'src/core/models/tour.model';
import { TOURS_LIST } from 'src/core/mocks/tours.mock';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tour-active',
  templateUrl: './tour-active.page.html',
  styleUrls: ['./tour-active.page.scss'],
})
export class TourActivePage implements OnInit {
  public tour: TourModel;

  constructor(
    private sessionTour: TourSessionService,
    public alertController: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
    this.tour = this.sessionTour.tour;
    // this.tour = TOURS_LIST[0];
  }

  public async quitTour() {
    const alert = await this.alertController.create({
      header: '¿Desea salir del tour?',
      // subHeader: 'Subtitle',
      message: 'el cobro ya ha sido realizado y no tiene devolución.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Salir',
          handler: () => this.cancelTour()
        }
      ]
    });

    await alert.present();
  }

  private cancelTour() {
    this.sessionTour.removeActivedTour();
    this.router.navigate(['../']);
  }

}
