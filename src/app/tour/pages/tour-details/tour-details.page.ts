import { Component, OnInit } from '@angular/core';
import { TourModel } from 'src/core/models/tour.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TOURS_LIST } from 'src/core/mocks/tours.mock';
import { AlertController, PopoverController } from '@ionic/angular';
import { TourSessionService } from 'src/core/services/tour-session.service';
import { CommentModel } from 'src/core/models/comment.model';
import { COMMENTS_LIST } from 'src/core/mocks/comments.mock';

@Component({
  selector: 'app-tour-details',
  templateUrl: './tour-details.page.html',
  styleUrls: ['./tour-details.page.scss'],
})
export class TourDetailsPage implements OnInit {
  public tour: TourModel;
  public canJoin: boolean;
  public comments: CommentModel[];

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly tourSession: TourSessionService,
    public popoverController: PopoverController,
    public alertController: AlertController,
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.tour = TOURS_LIST[id];
    this.comments = this.getComments();
    this.canJoin = this.tour.canJoin;
  }

  async joinOnTour() {
    const alert = await this.alertController.create({
      header: 'Ingresar',
      message: 'Se generara el codigo QR/NFC y se guardara su lugar en el tour, ademas se efectuara el pago correspondiente.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Unirse',
          handler: () => this.confirmJoin()
        }
      ]
    });

    await alert.present();
  }

  confirmJoin() {
    this.tourSession.setActivedTour(this.tour);
    this.router.navigate(['/tour']);
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: 'Bicicleta',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  getComments(): CommentModel[] {
    const ids = new Array<number>();
    const lot = Math.floor((Math.random() * 3) + 1);
    for (let i = 1; i <= lot; i++) {
      ids.push(Math.floor((Math.random() * COMMENTS_LIST.length) + 1));
    }
    return COMMENTS_LIST.filter(cmnt => ids.includes(cmnt.id));
  }

}
