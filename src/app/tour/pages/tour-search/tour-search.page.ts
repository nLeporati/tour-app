import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tour-search',
  templateUrl: './tour-search.page.html',
  styleUrls: ['./tour-search.page.scss'],
})
export class TourSearchPage implements OnInit {
  public searching: boolean;

  constructor(
    private readonly router: Router
  ) { }

  ngOnInit() {
  }

  onSearch(search = true) {
    this.searching = search;
  }

  closePage() {
    this.router.navigate(['/tour']);
  }

}
