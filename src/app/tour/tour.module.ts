import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { QRCodeModule } from 'angularx-qrcode';

import { IonicModule } from '@ionic/angular';

import { TourPage } from './tour.page';
import { TourDetailsPage } from './pages/tour-details/tour-details.page';
import { TourFeatureListComponent } from './components/tour-feature-list/tour-feature-list.component';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';
import { TourActivePage } from './pages/tour-active/tour-active.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { MapInfoWindowsComponent } from './components/map-info-windows/map-info-windows.component';
import { TourSearchPage } from './pages/tour-search/tour-search.page';
import { TourSearchBarComponent } from './components/tour-search-bar/tour-search-bar.component';

const routes: Routes = [
  {
    path: '',
    component: TourPage,
  },
  {
    path: 'active',
    component: TourActivePage
  },
  {
    path: 'search',
    component: TourSearchPage
  },
  {
    path: 'details/:id',
    component: TourDetailsPage
  }
];

@NgModule({
  providers: [
    QRScanner,
    Geolocation
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QRCodeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    TourFeatureListComponent,
    MapInfoWindowsComponent,
    TourSearchBarComponent,
    TourPage,
    TourDetailsPage,
    TourActivePage,
    TourSearchPage
  ]
})
export class TourPageModule {}
