import { Component, OnInit, Input } from '@angular/core';
import { TourModel } from 'src/core/models/tour.model';
import { TOURS_LIST } from 'src/core/mocks/tours.mock';

@Component({
  selector: 'app-tour-feature-list',
  templateUrl: './tour-feature-list.component.html',
  styleUrls: ['./tour-feature-list.component.scss'],
})
export class TourFeatureListComponent implements OnInit {
  public data: TourModel[];
  @Input() title: string;
  @Input() subtitle: string;

  constructor() { }

  ngOnInit() {
    this.data = TOURS_LIST;
  }

}
