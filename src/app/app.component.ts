import { Component, ViewChild } from '@angular/core';

import { Platform, IonMenuToggle, IonMenu } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TourSessionService } from 'src/core/services/tour-session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Mapa',
      url: '/tour',
      icon: 'pin'
    },
    {
      title: 'Mis tours',
      url: '/guide',
      icon: 'planet'
    },
    {
      title: 'Usuario',
      url: '/list',
      icon: 'contact'
    },
    {
      title: 'Tarjeta',
      url: '/list',
      icon: 'card'
    },
    {
      title: 'Ajustes',
      url: '/list',
      icon: 'settings'
    }
  ];

  @ViewChild(IonMenu) menu: IonMenu;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private router: Router,
    private statusBar: StatusBar,
    public sessionTour: TourSessionService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  goActiveTour() {
    this.menu.close();
    this.router.navigate(['/tour/active']);
  }
}
