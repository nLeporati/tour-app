import {Injectable} from '@angular/core';
import {HammerGestureConfig} from '@angular/platform-browser';
import * as Hammer from 'hammerjs';

/**
 * @hidden
 * This class overrides the default Angular gesture config.
 */
@Injectable()
export class IonicGestureConfig extends HammerGestureConfig {
    overrides =  {
        'swipe': { direction: Hammer.DIRECTION_ALL }
    };
    buildHammer(element: HTMLElement) {
        const mc = new (window as any).Hammer(element);

        for (const eventName in this.overrides) {
            if (eventName) {
                mc.get(eventName).set(this.overrides[eventName]);
            }
        }

        return mc;
    }
}
