import { TestBed } from '@angular/core/testing';

import { TourSessionService } from './tour-session.service';

describe('TourSessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TourSessionService = TestBed.get(TourSessionService);
    expect(service).toBeTruthy();
  });
});
