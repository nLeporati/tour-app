import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ILatLng } from '@ionic-native/google-maps';
import { HTTP } from '@ionic-native/http/ngx';
import { Observable, Subject } from 'rxjs';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { TourModel } from '../models/tour.model';
import { UserModel } from '../models/user.model';

interface DirectionResponse {
    geocoded_waypoints: any;
    routes: any;
}

@Injectable({
  providedIn: 'root'
})
export class MapService {
    private key: string;
    private itemDoc: AngularFirestoreDocument<any>;

    constructor(
        private readonly http: HTTP,
        private readonly _http: HttpClient,
        private readonly afs: AngularFirestore,
    ) {
        this.key = 'AIzaSyDqQvFnA0WoGznZOFSIwJBDtSexTsGQlFk';
        this.itemDoc = this.afs.doc<any>('map_users/' + environment.user.fid);
    }

    public getDirection(origin: ILatLng, dest: ILatLng): Observable<any> {
        const url = 'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/directions/json?';
        return this._http.get<any>(`${url}origin=${origin.lat},${origin.lng}&destination=${dest.lat},${dest.lng}&key=${this.key}`);
    }

    public async getDirectionNative(origin: ILatLng, destination: ILatLng): Promise<any> {
        try {
            let url = 'https://maps.googleapis.com/maps/api/directions/json?';
            url += `origin=${origin.lat},${origin.lng}&destination=${origin.lat},${origin.lng}&key=${this.key}`
            const params = {};
            const headers = {};
            const response = await this.http.get(url, params, headers);
            return response;

          } catch (error) {
            console.error(error);
          }
    }

    public async getDirectionAsync(origin: ILatLng, dest: ILatLng): Promise<ILatLng[]> {
        try {
            const url = 'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/directions/json?';
            const resp = await this._http.get<DirectionResponse>(`
              ${url}origin=${origin.lat},${origin.lng}&destination=${dest.lat},${dest.lng}&mode=walking&key=${this.key}`
            ).toPromise();
            const points = new Array<ILatLng>();
            resp.routes.forEach(route => {
              route.legs.forEach(leg => {
                leg.steps.forEach(step => {
                  points.push(step.start_location, step.end_location);
                });
              });
            });
            return points;

          } catch (error) {
            console.error(error);
          }
    }

    public updatePosition(user: any): Promise<void> {
      return this.itemDoc.update(user);
    }

    public getTour(fid: string): Observable<any> {
      return this.afs.doc<any>(`map_tours/${fid}`).valueChanges();
    }

    public addUserToTour(tour: TourModel, user: UserModel) {
      this.afs.doc<any>(`map_tours/${tour.fid}`).update({status: 'JOINED'});
      const data = this.afs.doc<any>(`map_tours/${tour.fid}`)
        .collection('users', ref => ref.where('user_id', '==', user.id)).snapshotChanges();


      data.subscribe(
        res => {
          res.forEach(usr => {
            console.log(usr);
            usr.payload.doc.ref.update({status: 'READY'});
          });
        }
      );
    }

    public updateTourStatus(tour: TourModel): Promise<void> {
      return this.afs.doc<any>(`map_tours/${tour.fid}`).update({ status: tour.status });
    }
}
