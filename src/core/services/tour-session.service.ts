import { Injectable } from '@angular/core';
import { TourModel } from '../models/tour.model';
import { TourSessionModel } from '../models/tour-session.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TourSessionService {
  public tour: TourModel;
  public active: boolean;
  public status: string;
  private fid: string;
  private user = environment.user;

  constructor(private afs: AngularFirestore) { }

  public setActivedTour(tour: TourModel) {
    this.tour = tour;
    this.active = true;
    this.status = 'join';
    this.afs.doc<any>(`map_users/${this.user.fid}`).update({status: 'JOINED'});
    this.afs.doc<any>(`map_tours/${tour.fid}`)
            .collection('users')
            .add({map_user: `map_users/${this.user.fid}`, user_id: this.user.id})
            .then(
              res => {
                this.fid = res.id;
              }
            );
  }

  public removeActivedTour() {
    this.afs.doc<any>(`map_tours/${this.tour.fid}`)
            .collection('users')
            .doc(this.fid)
            .delete();
    this.tour = null;
    this.active = false;
    this.status = '';
  }

  public updateTourStatus(st: string): Promise<void> {
    return this.afs.doc<any>(`map_tours/${this.tour.fid}`).update({ status: st });
  }

  public watchTour(): Observable<any> {
    return this.afs.doc<any>(`map_tours/${this.tour.fid}`).valueChanges();
  }

}
