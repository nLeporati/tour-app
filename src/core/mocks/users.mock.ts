import { UserModel } from '../models/user.model';

export const USERS_LIST: UserModel[] = [
  {
    id: 1,
    name: 'Nicolás Leporati',
    username: 'nicolas',
    email: 'nicolasl@kmeet.com',
    role: ['admin'],
    fid: 'lf7gGe3zf73L9sAbnLFb'
  },
  {
    id: 2,
    name: 'Daniel Salazar',
    username: 'daniel',
    email: 'daniels@kmeet.com',
    role: ['guide']
  },
  {
    id: 3,
    name: 'Rocio Mandela',
    username: 'rocio',
    email: 'rociom@kmeet.com',
    role: ['guide']
  },
  {
    id: 4,
    name: 'Juan Peréz',
    username: 'juan',
    email: 'juanp@email.com',
    role: ['turist'],
    fid: 'RgmdmOguiicSMzEjvOYH'
  },
  {
    id: 5,
    name: 'Miguel Fuentealba',
    username: 'miguel',
    email: 'miguelf@email.com',
    role: ['turist']
  },
  {
    id: 6,
    name: 'Catalina Saéz',
    username: 'catalina',
    email: 'catalinas@kmeet.com',
    role: ['turist']
  },
  {
    id: 7,
    name: 'Kasandra Menáres',
    username: 'kasandra',
    email: 'kasandram@email.com',
    role: ['turist']
  },
];
