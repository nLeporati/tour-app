import { USERS_LIST } from './users.mock';
import { CommentModel } from '../models/comment.model';

export const COMMENTS_LIST: CommentModel[] = [
  {
    id: 1,
    user: USERS_LIST[3],
    comment: 'Me parece un muy buen tour, quizas podria mejorar su ingles y seria perfecto, muchas felicitaciones.',
    rating: 4
  },
  {
    id: 2,
    user: USERS_LIST[4],
    comment: 'Excelente!',
    rating: 5
  },
  {
    id: 3,
    user: USERS_LIST[5],
    comment: 'No me parecieron tan atractivos los lugares y falta mas experticie por parte del guía, no lo recomiendo.',
    rating: 2
  },
  {
    id: 4,
    user: USERS_LIST[6],
    comment: 'Muy buenas actividades, me gusto el paseo en el museo y el juego de futbol!',
    rating: 5
  },
  {
    id: 5,
    user: USERS_LIST[3],
    comment: 'Es un tour muy interestante, se aprende un monton!',
    rating: 5
  }
];
