import { TourModel } from '../models/tour.model';
import { USERS_LIST } from './users.mock';

const img = 'http://lorempixel.com/960/634/city/';

export const TOURS_LIST: TourModel[] = [
    {
        id: 0,
        title: 'In Santiago',
        description: 'Un lindo tour',
        duration: 60,
        location: 'Plaza de Armas',
        type: 'bicycle',
        images: ['assets/images/santiago/plaza-armas.jpg'],
        status: 'ACTIVE',
        canJoin: true,
        guide: USERS_LIST[1],
        position: { lat: -33.446528, lng: -70.610957 },
        tags: [
            {name: 'city'},
            {name: 'culture'},
            {name: 'santiago'},
            {name: 'chile'},
            {name: 'center'},
            {name: 'funny'}
        ],
        itinerary: [
            { id: 1, name: 'Palacio de la Moneda' },
            { id: 1, name: 'Paseo Bulnes' }
        ],
        participants: 5
    },
    {
        id: 1,
        title: 'Tour 23',
        description: 'Un lindo tour',
        duration: 60,
        location: 'Baquedano',
        type: 'walk',
        images: ['assets/images/santiago/plaza-baquedano.jpg'],
        status: 'ACTIVE',
        guide: USERS_LIST[1],
        position: { lat: -33.449223, lng: -70.616410 },
        tags: [
            {name: 'city'},
            {name: 'culture'},
            {name: 'chile'},
        ],
        itinerary: [
            { id: 1, name: 'Palacio de la Moneda' },
            { id: 1, name: 'Paseo Bulnes' }
        ]
    },
    {
        id: 2,
        title: 'Maipo Caminata',
        description: 'Un lindo tour',
        duration: 60,
        location: 'Cajón del Maipo',
        type: 'walk',
        images: ['assets/images/santiago/maipo.jpg'],
        status: 'ACTIVE',
        guide: USERS_LIST[1],
        position: { lat: -33.449223, lng: -70.616410 },
        tags: [
            {name: 'city'},
            {name: 'culture'},
            {name: 'funny'},
            {name: 'natural'}
        ],
        itinerary: [
            { id: 1, name: 'Palacio de la Moneda' },
            { id: 1, name: 'Paseo Bulnes' }
        ]
    },
    {
        id: 3,
        title: 'Bellavista',
        description: 'Un lindo tour',
        duration: 60,
        location: 'Renca',
        type: 'bicycle',
        images: ['assets/images/santiago/bellavista.jpg'],
        status: 'ACTIVE',
        guide: USERS_LIST[2],
        position: { lat: -33.458036, lng: -70.606962 },
        tags: [
            {name: 'city'},
            {name: 'national'},
            {name: 'funny'},
            {name: 'party'},
        ],
        itinerary: [
            { id: 1, name: 'Palacio de la Moneda' },
            { id: 1, name: 'Paseo Bulnes' }
        ]
    },
    {
        id: 4,
        title: 'Paseo Yungay',
        description: 'Un lindo tour',
        duration: 60,
        location: 'Barrio Yungay',
        type: 'car',
        images: ['assets/images/santiago/Barrio-Yungay.jpg'],
        status: 'INACTIVE',
        canJoin: true,
        guide: USERS_LIST[2],
        position: { lat: -33.459061, lng: -70.701071 },
        tags: [
            {name: 'national'},
            {name: 'culture'},
            {name: 'funny'}
        ],
        itinerary: [
            { id: 1, name: 'Palacio de la Moneda' },
            { id: 1, name: 'Paseo Bulnes' }
        ]
    },
    {
        id: 5,
        title: 'Barrio Italia',
        description: 'Un lindo tour',
        duration: 60,
        location: 'Ñuñoa',
        type: 'bicycle',
        images: ['assets/images/santiago/italia.jpg'],
        status: 'INACTIVE',
        guide: USERS_LIST[2],
        position: { lat: -33.456725, lng: -70.695251 },
        tags: [
            {name: 'city'},
            {name: 'culture'},
            {name: 'national'},
            {name: 'italy'},
        ],
        itinerary: [
            { id: 1, name: 'Palacio de la Moneda' },
            { id: 1, name: 'Paseo Bulnes' },
            { id: 1, name: 'Torre Entel' }
        ]
    },
    {
        id: 6,
        fid: 'PbnGywO3mQbC3WRPUo2P',
        title: 'Provi Tour',
        description: 'Un lindo tour',
        duration: 60,
        location: 'Providencia',
        type: 'walk',
        images: ['assets/images/santiago/General-Holley.jpg'],
        status: 'INACTIVE',
        canJoin: true,
        guide: USERS_LIST[2],
        position: { lat: -33.440907, lng: -70.652287 },
        tags: [
            {name: 'city'},
            {name: 'shopping'},
            {name: 'funny'},
            {name: 'builds'},
        ],
        itinerary: [
            { id: 1, name: 'Palacio de la Moneda' },
            { id: 1, name: 'Paseo Bulnes' }
        ]
    },
  ];
