import { TourModel } from './tour.model';

export class TourSessionModel {
    tour: TourModel;
    active: boolean;
    status: string;
}
