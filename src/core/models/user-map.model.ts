import { ILatLng } from '@ionic-native/google-maps';

export class UserMapModel {
  position: ILatLng;
  status?: string;
}
