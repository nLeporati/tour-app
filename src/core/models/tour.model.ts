import { TourTag } from './tour-tag.model';
import { PlaceModel } from './place.model';
import { ILatLng } from '@ionic-native/google-maps';
import { UserModel } from './user.model';

export class TourModel {
    id: number;
    fid?: string;
    guide: UserModel;
    title: string;
    description: string;
    duration: number;
    location: string;
    type: string;
    images: string[];
    status: string;
    canJoin?: boolean;
    tags?: TourTag[];
    itinerary?: PlaceModel[];
    position?: ILatLng;
    participants?: number;
}
