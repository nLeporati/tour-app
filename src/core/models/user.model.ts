export class UserModel {
  id: number;
  username: string;
  email: string;
  name: string;
  role?: string[];
  image?: string;
  fid?: string;
  status?: string;
}
